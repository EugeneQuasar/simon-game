# Simon game

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```


Игра Simon направленная на развитие и тренировку памяти и скорости реакции.
Возможности:
- Генерация случайной последовательности цветов.
- 3 уровня сложности(скорости воспроизведения).
- анимация подсвечивания.
- проигрывание звукового сигнала(при выборе, победе, поражении).
- автоматические уведомления о правильности ввода цветов.
- вывод информации состояния игры.
- подсчет раунда.
